﻿namespace Frostbolt.Domain.Sets
{
    public enum Color
    {
        blue,
        green,
        red,
        white
    }
}
