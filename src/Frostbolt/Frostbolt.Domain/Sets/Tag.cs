﻿namespace Frostbolt.Domain.Sets
{
    public enum Tag
    {
        Spell,
        Projectile,
        Cold,
        AoE,
        Nova,
        Duration,
        Physical,
        Orb,
        Attack,
        Chaos,
        Trap,
        Melee,
        Strike,
        Lightning,
        Fire,
        Chaining,
        Prismatic,
        Curse,
        Hex,
        Support
    }
}
