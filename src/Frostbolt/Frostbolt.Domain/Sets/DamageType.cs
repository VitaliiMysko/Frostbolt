﻿namespace Frostbolt.Domain.Sets
{
    public enum DamageType
    {
        cold,
        physical,
        chaos,
        lightning,
        fire,
        elemental
    }
}