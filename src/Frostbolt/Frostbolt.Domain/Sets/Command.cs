﻿namespace Frostbolt.Domain.Sets
{
    public enum Command
    {
        Equip,
        Setgem,
        Setbutton,
        Press,
        Opponent,
        Characters
    }
}
