﻿using Frostbolt.Domain.Entities;

namespace Frostbolt.Domain.Interfaces
{
    public interface ICommand
    {
        public Report Execute();
    }
}
