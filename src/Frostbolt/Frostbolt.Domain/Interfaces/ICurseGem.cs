﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frostbolt.Domain.Interfaces
{
    interface ICurseGem
    {
        ICurse Curse { get; }
        void SetCurse(ICurse curse);
    }
}
