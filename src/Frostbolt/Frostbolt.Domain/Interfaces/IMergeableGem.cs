﻿using Frostbolt.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frostbolt.Domain.Interfaces
{
    public interface IMergeableGem
    {
        bool IsMergeable();
        void Merge(SkillGem skillGem);
    }
}
