﻿
namespace Frostbolt.Domain.Interfaces
{
    public interface IOutput
    {
        public void Print(string message);
    }
}
