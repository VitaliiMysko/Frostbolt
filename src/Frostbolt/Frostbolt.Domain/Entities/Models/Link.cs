﻿using Frostbolt.Domain.Interfaces;
using Frostbolt.Domain.Sets;
using System.Collections.Generic;
using System.Linq;

namespace Frostbolt.Domain.Entities
{
    public class Link
    {
        public Gem[] Gems { get; }
        public List<SkillGem> Skills
        {
            get => CalculateSkills();

            private set { }
        }


        public Link(Gem[] gems)
        {
            Gems = gems;
        }

        private List<SkillGem> CalculateSkills()
        {
            List<SkillGem> skills = new();

            var skillGroups = GetSkillGroups(Gems);

            foreach (var skillGroup in skillGroups)
            {
                //find skill gem in group
                SkillGem skillGem = (SkillGem)skillGroup.Find(i => i is SkillGem);

                SkillGem skill = (SkillGem)skillGem.Clone();

                //get all support gems
                List<SupportGem> supportGems = new();
                foreach (var gem in skillGroup)
                {
                    if (gem is SupportGem)
                    {
                        supportGems.Add((SupportGem)gem);
                    }
                }

                //empower must be first
                var empowerGems = supportGems.Where(i => i is EmpowerSupport);
                foreach (var gem in empowerGems)
                {
                    gem.AddEffect(skill);
                }

                supportGems.RemoveAll(i => i is EmpowerSupport);

                //then another support effect
                foreach (var gem in supportGems)
                {
                    gem.AddEffect(skill);
                }

                //save base damage
                skill.BaseDamage = new();
                foreach (var damage in skill.Damage)
                {
                    skill.BaseDamage.Add(new Damage { Type = damage.Type, Amount = damage.Amount });
                }

                skills.Add(skill);
            }

            SetIsActive(skills, Gems);
            MergeCurseGems(skills);
            skills = RemoveDisabledSkills(skills);

            return skills;
        }

        private void SetIsActive(List<SkillGem> skills, Gem[] Gems)
        {
            foreach(var skill in skills)
            {
                skill.SetIsActive(Gems, Hero.GetInstance());
            }
        }

        private List<List<Gem>> GetSkillGroups(Gem[] gems)
        {
            var groups = GetGroupsByTags(gems);

            return groups;
        }

        private List<SkillGem> RemoveDisabledSkills(List<SkillGem> skills)
        {
            return skills.Where(i => i.IsActive).ToList();
        }

        private List<List<Gem>> GetGroupsByTags(Gem[] links)
        {
            List<Gem> skillLinks = new List<Gem>();
            List<Gem> supportLinks = new List<Gem>();

            foreach (var link in links)
            {
                if (link == null)
                    continue;

                if (link.Tags.FindIndex(t => t == Tag.Support) < 0)
                {
                    skillLinks.Add(link);
                    continue;
                }
                supportLinks.Add(link);
            }


            List<List<Gem>> skillGroups = new();

            foreach (var skillLink in skillLinks)
            {
                List<Gem> gemLink = new() { skillLink };

                var skillTags = skillLink.Tags;

                foreach (var supportLink in supportLinks)
                {
                    var isLink = true;

                    foreach (var supportTag in supportLink.Tags)
                    {
                        if (supportTag == Tag.Support)
                            continue;

                        if (skillTags.FindIndex(t => t == supportTag) < 0)
                        {
                            isLink = false;
                            break;
                        }
                    }

                    if (isLink)
                    {
                        gemLink.Add(supportLink);
                    }

                }

                skillGroups.Add(gemLink);
            }

            return skillGroups;
        }


        private void MergeCurseGems(List<SkillGem> skills)
        {
            foreach (var mergeGem in skills.Where(i => i is ICurseGem))
            {
                var mergeGemTemp = (IMergeableGem)mergeGem;
                var curseGemTemp = (ICurseGem)mergeGem;
                foreach (var skill in skills)
                {
                    if (curseGemTemp.Curse == null) continue;

                    mergeGemTemp.Merge(skill);
                }
            }
        }
    }
}