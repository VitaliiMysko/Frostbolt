﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Frostbolt.Domain.Entities
{
    public class Hero
    {
        private static Hero _instance;
        Random _rand = new Random();

        public static Hero GetInstance()
        {
            if (_instance == null)
                _instance = new Hero();
            return _instance;
        }

        public Weapon Weapon;
        public BodyArmour BodyArmour;
        public Flask[] Flasks = new Flask[5];

        public int GetFlasksPower()
        {
            int sum = 0;
            foreach (var flask in Flasks)
            {
                if(flask != null)
                sum += flask.Power;
            }
            return sum;
        }

        public List<SkillGem> GetSkills()
        {
            var skills = new List<SkillGem>();
            Gem[] gem = new Gem[BodyArmour.Slots.Length];

            var gemList = new List<Gem>();
            BodyArmour.Slots.ToList().Where(i => i.Gem != null).ToList().ForEach(i => gemList.Add((Gem)i.Gem.Clone()));

            //add lever from equipments
            gem = gemList.ToArray();
            AddLevel(gem);

            //Get skills from link
            var link = new Link(gem);
            skills = link.Skills;

            //add equipments parameters
            AddEquipmentsParameters(skills);

            return skills;
        }

        private void AddEquipmentsParameters(List<SkillGem> skills)
        {
            if (BodyArmour != null)
                skills.ForEach(i => BodyArmour.AddEffect(i));

            if(Weapon != null)
            skills.ForEach(i => Weapon.AddEffect(i));
        }


        public void Attack(SkillGem skill, Opponent opponent)
        {
            var casts = skill.Parameters.Find(x => x.Name == "casts");

            int probabilityCast = 75;

            CastSpell(skill, opponent);

            if (casts == null) return;

            for (int i = 1; i < casts.Count; i++)
            {
                if (_rand.Next(0, 100 + 1) < probabilityCast)
                {
                    CastSpell(skill, opponent);
                }
            }
        }

        private void CastSpell(SkillGem skill, Opponent opponent)
        {
            int probabilityProjectile = 50;
            var projectiles = skill.Parameters.Find(x => x.Name == "projectiles");

            ShotProjectile(skill, opponent);

            if (projectiles == null) return;

            for (int i = 1; i < projectiles.Count; i++)
            {
                if (_rand.Next(0, 100 + 1) < probabilityProjectile)
                {
                    ShotProjectile(skill, opponent);
                }
            }
        }

        private void ShotProjectile(SkillGem skillGem, Opponent opponent)
        {
            opponent.GetHit(skillGem);
        }


        public string GetDescription()
        {
            var description = "Hero:";

            if (BodyArmour != null)
            {
                description += $"\n{BodyArmour.GetDescription()}";
            }
            else
            {
                description += $"\n BodyArmour: X;";
            }


            if (Weapon != null)
            {
                description += $"\n{Weapon.GetDescription()}";
            }
            else
            {
                description += $"\n Weapon: X;";
            }


            var flaskDescription = "";

            foreach (var flask in Flasks)
            {
                if (flask == null || string.IsNullOrEmpty(flask.Name))
                    continue;

                flaskDescription += $"{flask.Name}; ";
            }

            if (string.IsNullOrEmpty(flaskDescription))
            {
                flaskDescription = " Flasks: X;";
            }
            else
            {
                flaskDescription = $" Flasks: {flaskDescription}";
            }

            description += $"\n{flaskDescription}";

            return description;
        }


        private void AddLevel(Gem[] gem)
        {
            if (BodyArmour != null)
                BodyArmour.AddLevel(gem);

            if(Weapon != null)
            Weapon.AddLevel(gem);
        }
    }
}
