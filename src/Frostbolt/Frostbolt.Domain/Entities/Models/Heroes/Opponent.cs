﻿using Frostbolt.Domain.Sets;
using System.Collections.Generic;
using System.Linq;

namespace Frostbolt.Domain.Entities
{
    public class Opponent
    {
        private static Opponent _instance;

        public static Opponent GetInstance()
        {
            if (_instance == null)
                _instance = new Opponent();
            return _instance;
        }

        public int Life { get; set; }
        public int FireResistance { get; set; }
        public int LightningResistance { get; set; }
        public int ColdResistance { get; set; }
        public int PhysicalResistance { get; set; }
        public int ChaosResistance { get; set; }
        public List<Parameter> Parameters { get; set; } = new();

        public void GetHit(SkillGem skillGem)
        {
            SetResistance(skillGem.Parameters);

            int damage = skillGem.Damage.Sum(x => GetDamage(x));

            Life = Life - damage > 0 ? Life - damage : 0;
        }

        private int GetDamage(Damage damage)
        {
            if (damage == null)
                return 0;

            switch (damage.Type)
            {
                case DamageType.cold:
                    return (int)(damage.Amount * ((double)(100 - ColdResistance) / 100));
                case DamageType.physical:
                    return (int)(damage.Amount * ((double)(100 - PhysicalResistance) / 100));
                case DamageType.chaos:
                    return (int)(damage.Amount * ((double)(100 - ChaosResistance) / 100));
                case DamageType.lightning:
                    return (int)(damage.Amount * ((double)(100 - LightningResistance) / 100));
                case DamageType.fire:
                    return (int)(damage.Amount * ((double)(100 - FireResistance) / 100));
                case DamageType.elemental:
                    var elementalDamage = (ElementalDamage)damage;
                    var newDamage = new Damage { Type = elementalDamage.GetDamageType(), Amount = damage.Amount };
                    elementalDamage.Rotate();
                    return GetDamage(newDamage);
                default:
                    return 0;
            }

        }

        private void SetResistance(List<Parameter> parameters)
        {
            parameters.ForEach(p =>
            {
                if (p.Name.Contains("vulnerability"))
                {
                    SetBasicResistance();
                    var vulnerabilityType = p.Name.Split(" ")[0].ToLower().Trim();
                    var vulnerabilityCount = (int)p.Count;

                    switch (vulnerabilityType)
                    {
                        case nameof(DamageType.cold):
                            ColdResistance -= vulnerabilityCount;
                            break;
                        case nameof(DamageType.physical):
                            PhysicalResistance -= vulnerabilityCount;
                            break;
                        case nameof(DamageType.chaos):
                            ChaosResistance -= vulnerabilityCount;
                            break;
                        case nameof(DamageType.lightning):
                            LightningResistance -= vulnerabilityCount;
                            break;
                        case nameof(DamageType.fire):
                            FireResistance -= vulnerabilityCount;
                            break;
                        default:
                            break;
                    }
                }
            });

            Parameters.AddRange(parameters.Where(p=>p.Name.Contains("vulnerability")));
        }

        private void SetBasicResistance()
        {
            foreach(var param in Parameters)
            {
                if (param.Name.Contains("vulnerability"))
                {
                    var vulnerabilityType = param.Name.Split(" ")[0].ToLower().Trim();
                    var vulnerabilityCount = (int)param.Count;

                    switch (vulnerabilityType)
                    {
                        case nameof(DamageType.cold):
                            ColdResistance += vulnerabilityCount;
                            break;
                        case nameof(DamageType.physical):
                            PhysicalResistance += vulnerabilityCount;
                            break;
                        case nameof(DamageType.chaos):
                            ChaosResistance += vulnerabilityCount;
                            break;
                        case nameof(DamageType.lightning):
                            LightningResistance += vulnerabilityCount;
                            break;
                        case nameof(DamageType.fire):
                            FireResistance += vulnerabilityCount;
                            break;
                        default:
                            break;
                    }
                }
            }

            Parameters.RemoveAll(p => p.Name.Contains("vulnerability"));
        }

        public string GetDescription()
        {
            var description = "Opponent: ";

            description += $"\n Life: {Life} ";
            description += $"\n Cold resistance: {ColdResistance}; ";
            description += $"\n Fire resistance: {FireResistance}; ";
            description += $"\n Chaos resistance: {ChaosResistance}; ";
            description += $"\n Lightning resistance: {LightningResistance}; ";
            description += $"\n Physical resistance: {PhysicalResistance}; ";

            return description;
        }

    }
}
