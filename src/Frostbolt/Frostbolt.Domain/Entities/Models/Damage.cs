﻿using Frostbolt.Domain.Sets;
using System;

namespace Frostbolt.Domain.Entities
{
    public class Damage : ICloneable
    {
        public DamageType Type;
        public int Amount;

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}