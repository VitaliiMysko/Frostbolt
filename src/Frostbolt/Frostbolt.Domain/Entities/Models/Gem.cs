﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;

namespace Frostbolt.Domain.Entities
{
    public abstract class Gem : ICloneable
    {
        public string Name;
        public List<Tag> Tags { protected set; get; } = new List<Tag>();
        public Color Color { protected set; get; }
        public List<Parameter> Parameters { protected set; get; } = new List<Parameter>();
        public int lvl { protected set; get; }
        public int quality { protected set; get; }
        public abstract void AddLevelReseting(int count);

        public virtual object Clone()
        {
            Gem gemClone =  (Gem)this.MemberwiseClone();
            gemClone.Parameters = new();
            foreach(var param in Parameters)
            {
                gemClone.Parameters.Add(new Parameter { Name = param.Name, Count = param.Count});
            }

            return (object)gemClone;
        }
    }
}
