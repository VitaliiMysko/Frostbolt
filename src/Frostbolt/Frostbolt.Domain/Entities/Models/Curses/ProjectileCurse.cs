﻿using Frostbolt.Domain.Interfaces;
using System.Linq;

namespace Frostbolt.Domain.Entities
{
    public class ProjectileCurse : ICurse
    {
        public bool IsCursable(SkillGem skillGem)
        {
            return skillGem.Tags.Any(i=>i == Sets.Tag.Projectile);
        }
    }
}
