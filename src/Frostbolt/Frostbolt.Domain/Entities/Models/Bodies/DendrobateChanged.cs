﻿using Frostbolt.Domain.Sets;

namespace Frostbolt.Domain.Entities
{
    public class DendrobateChanged : BodyArmour
    {
        public readonly int Lvl; //???
        public readonly int ChanceToPoison;
        public readonly double IncreasedChaosDamagePercent;  

        public DendrobateChanged()
        {
            this.Name = "Dendrobate Changed";
            this.ChanceToPoison = 10;
            this.IncreasedChaosDamagePercent = 80;
            this.Slots = GetSlots();
        }

        private Slot[] GetSlots()
        {
            Slots = new Slot[6];

            Slots[0] = new Slot() { Color = Color.red, Number = 1 };
            Slots[1] = new Slot() { Color = Color.green, Number = 2 };
            Slots[2] = new Slot() { Color = Color.green, Number = 3 };
            Slots[3] = new Slot() { Color = Color.blue, Number = 4 };
            Slots[4] = new Slot() { Color = Color.green, Number = 5 };
            Slots[5] = new Slot() { Color = Color.green, Number = 6 };

            return Slots;
        }


        public override void AddEffect(SkillGem skill)
        {
            //--14.02.22
            var chaosDamage = skill.Damage.Find(i => i.Type == DamageType.chaos);
            var baseCaosDamage = skill.BaseDamage.Find(i => i.Type == DamageType.chaos);
            if (chaosDamage != null || baseCaosDamage != null)
            {
                chaosDamage.Amount += (int)(baseCaosDamage.Amount * 0.8);
            }
        }
    }
}
