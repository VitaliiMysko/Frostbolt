﻿using Frostbolt.Domain.Sets;
using System.Linq;

namespace Frostbolt.Domain.Entities
{
    public class CorruptionSanctuary : BodyArmour
    {
        public readonly int IncreaseLightningDamagePercent;
        public readonly int LightningDamageToAttacks;
        public readonly int ToLevelOfSocketedGems;
        public readonly int ToLevelOfAllSupportGems;

        public CorruptionSanctuary()
        {
            this.Name = "Corruption Sanctuary";
            this.IncreaseLightningDamagePercent = 40;
            this.LightningDamageToAttacks = 3;
            this.ToLevelOfSocketedGems = 3;
            this.ToLevelOfAllSupportGems = 3;
            this.Slots = GetSlots();
        }

        private Slot[] GetSlots()
        {
            Slots = new Slot[3];

            Slots[0] = new Slot() { Color = Color.blue, Number = 1 };
            Slots[1] = new Slot() { Color = Color.red, Number = 2 };
            Slots[2] = new Slot() { Color = Color.blue, Number = 3 };

            return Slots;
        }

        public override void AddLevel(Gem[] gems)
        {
            foreach(var gem in gems)
            {
                gem.AddLevelReseting(3);
            }

            foreach(var gem in gems.ToList().Where(i=>i is SupportGem))
            {
                gem.AddLevelReseting(3);
            }
        }

        public override void AddEffect(SkillGem skill)
        {
            var damage = skill.Damage.Find(x => x.Type == DamageType.chaos);
            if (damage != null)
                damage.Amount = 0;

            var lightDamage = skill.Damage.Find(i=>i.Type == DamageType.lightning);
            var baseLightDamage = skill.BaseDamage.Find(i => i.Type == DamageType.lightning);
            if (lightDamage != null || baseLightDamage != null)
            {
                lightDamage.Amount += (int)(baseLightDamage.Amount * 0.4);
                lightDamage.Amount += 3;
            }
        }
    }
}
