﻿using Frostbolt.Domain.Sets;

namespace Frostbolt.Domain.Entities
{
    public class TabulaRasa : BodyArmour
    {
        public TabulaRasa()
        {
            this.Name = "Tabula Rasa";
            this.Slots = GetSlots();
        }

        private Slot[] GetSlots()
        {
            Slots = new Slot[6];

            Slots[0] = new Slot() { Color = Color.white, Number = 1 };
            Slots[1] = new Slot() { Color = Color.white, Number = 2 };
            Slots[2] = new Slot() { Color = Color.white, Number = 3 };
            Slots[3] = new Slot() { Color = Color.white, Number = 4 };
            Slots[4] = new Slot() { Color = Color.white, Number = 5 };
            Slots[5] = new Slot() { Color = Color.white, Number = 6 };

            return Slots;
        }

        public override void AddEffect(SkillGem skill) { }
    }
}
