﻿namespace Frostbolt.Domain.Entities
{
    public class BodiesBuilder
    {
        private readonly string _nameEquip;

        public BodiesBuilder(string nameEquip)
        {
            _nameEquip = nameEquip;
        }

        public BodyArmour Build()
        {
            if (_nameEquip == "tabula_rasa")
            {
                return new TabulaRasa();
            }
            else if (_nameEquip == "corruption_sanctuary")
            {
                return new CorruptionSanctuary();
            }
            else if (_nameEquip == "dendrobate_changed")
            {
                return new DendrobateChanged();
            }
            else
            {
                return null;
            }

        }

    }
}
