﻿using Frostbolt.Domain.Sets;
using System;

namespace Frostbolt.Domain.Entities
{
    public class Slot
    {
        private Gem _gem;

        public int Number;
        public Color Color;
        public Gem Gem
        {
            get => _gem;
            set
            {
                if (value.Color == Color || Color == Color.white)
                {
                    _gem = value;
                }
                else throw new ArgumentException("An incorrect gem is installed in the slot due to color");
            }
        }
    }
}
