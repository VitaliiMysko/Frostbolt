﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frostbolt.Domain.Entities
{
    public class ElementalDamage : Damage
    {
        DamageType curentType = DamageType.lightning;
        public Queue<DamageType> DamageTypesCycle = new();

        public void Rotate()
        {
            DamageTypesCycle.Enqueue(curentType);
            curentType = DamageTypesCycle.Dequeue();
        }
        public DamageType GetDamageType()
        {
            return curentType;
        }
    }
}
