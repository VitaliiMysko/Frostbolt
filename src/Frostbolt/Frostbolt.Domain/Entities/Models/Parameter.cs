﻿namespace Frostbolt.Domain.Entities
{
    public class Parameter
    {
        public string Name;
        public double Count;
    }
}