﻿namespace Frostbolt.Domain.Entities
{
    public abstract class BodyArmour
    {
        public string Name;
        public Slot[] Slots;

        public virtual void AddLevel(Gem[] gem) { }

        public abstract void AddEffect(SkillGem i);

        public virtual string GetDescription()
        {
            var description = $" BodyArmour: {Name};";

            var slotDescription = " Slots: ";

            if (Slots == null || Slots.Length == 0)
            {
                slotDescription += "X;";
            }
            else
            {
                foreach (var slot in Slots)
                {
                    if (slot == null)
                        continue;

                    var nameGem = slot.Gem != null ? slot.Gem.Name : "X";

                    slotDescription += $"\n  {slot.Number}: {nameGem}; ";
                }
            }

            description += $"\n {slotDescription}";

            return description;
        }
    }
}
