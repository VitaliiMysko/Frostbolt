﻿using Frostbolt.Domain.Sets;
using System.Linq;

namespace Frostbolt.Domain.Entities
{
    public class PledgeOfHands : Weapon
    {
        public readonly int QualityPercent;
        public readonly int PhysicalDamage;
        public readonly double CriticalStrikeChancePercent;
        public readonly double AttacksPerSecond;
        public readonly int WeaponRange;
        public readonly int RequiresLevel;
        public readonly int Str;
        public readonly int Int;
        public readonly int ChanceToBlockSpellDamagePercent;
        public readonly SpellEchoSupport GreaterSpellEcho;
        public readonly int IncreaseItsDamagePercent;
        public readonly int IncreaseMaximumManaPercent;

        public PledgeOfHands()
        {
            Name = "Pledge of Hands";

            PhysicalDamage = 100;
            CriticalStrikeChancePercent = 6.5;
            AttacksPerSecond = 1.3;
            GreaterSpellEcho = new SpellEchoSupport(30, 0);
            IncreaseItsDamagePercent = 148;
        }

        public override void AddEffect(SkillGem skill)
        {
            GreaterSpellEcho.AddEffect(skill);

            if(skill.Tags.Any(i=> i == Tag.Spell))
            {
                //--14.02.22
                foreach (var item in skill.Damage)
                { 
                    item.Amount += (int)(skill.BaseDamage.Find(i => i.Type == item.Type).Amount * 1.48);
                }
            }
        }
    }
}
