﻿using Frostbolt.Domain.Sets;
using System;
using System.Linq;

namespace Frostbolt.Domain.Entities
{
    public class DreadBane : Weapon
    {
        public readonly int PhysicalDamage;
        public readonly int CriticalStrikeChancePercent;
        public readonly double AttacksPerSecond;
        public readonly int WeaponRange;
        public readonly int ItemLevel;
        public readonly int RequiresLevel;
        public readonly int Str;
        public readonly int Int;
        public readonly int ChanceToBlockAtackDamagePercent;
        public readonly int IncreaseSpellDamagePercent;
        public readonly int AddsColdDamageToSpell;
        public readonly int IncreaseCastSpeedPercent;
        public readonly int IncreaseCriticalStrikeChanceForSpellsPercent;
        public readonly int ToMaximumMana;
        public readonly int ToAllLightningSkillGems;


        public DreadBane()
        {
            Name = "Dread Bane";
            PhysicalDamage = 100;
            CriticalStrikeChancePercent = 6;
            AttacksPerSecond = 1.3;
            IncreaseSpellDamagePercent = 15;
            AddsColdDamageToSpell = 56;
            IncreaseCastSpeedPercent = 13;
            IncreaseCriticalStrikeChanceForSpellsPercent = 69;
            ToAllLightningSkillGems = 2;
        }

        public override void AddEffect(SkillGem skill)
        {
            if(skill.Parameters.Any(i=>i.Name == "cast speed"))
            {
                var param = skill.Parameters.Find(i => i.Name == "cast speed").Count;

                param = param * 1.13;
                param = Math.Round(param, 2);
            }

            if (skill.Tags.Any(i => i == Tag.Spell))
            {
                var damage = skill.Damage;
                
                //--14.02.222
                foreach(var item in damage)
                {
                    var baseDamage = skill.BaseDamage.Find(i => i.Type == item.Type);
                    if (damage != null && baseDamage != null)
                        item.Amount += (int)(baseDamage.Amount * 1.15);
                }

                var damageCold = damage.Find(x => x.Type == DamageType.cold);

                if (damageCold != null)
                    damageCold.Amount += 56;
                else
                    skill.Damage.Add(new Damage { Type = DamageType.cold, Amount = 56 });
            }
        }
    }
}
