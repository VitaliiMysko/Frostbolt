﻿using Frostbolt.Domain.Sets;
using System.Linq;

namespace Frostbolt.Domain.Entities
{
    public class ColdIronPoint : Weapon
    {
        public readonly int PhysicalDamage;
        public readonly double CriticalStrikeChancePercent;
        public readonly double AttacksPerSecond;
        public readonly int WeaponRange;
        public readonly int RequiresLevel;
        public readonly int Str;
        public readonly int Dex;
        public readonly int Int;
        public readonly int AddsPhysicalDamageToSpells;
        public readonly bool DealNoLighthingDamage;
        public readonly int IncreaseGlobalCriticalStrikePercent;
        public readonly int ToLevelOfAllPhysicalSpellSkillGems;
        public readonly bool DealNoElementalDamage;
        public readonly bool Corrupted;

        public ColdIronPoint()
        {
            Name = "Cold Iron Point";
            PhysicalDamage = 27;
            CriticalStrikeChancePercent = 6.3;
            AttacksPerSecond = 1.4;
            AddsPhysicalDamageToSpells = 27;
            IncreaseGlobalCriticalStrikePercent = 30;
            ToLevelOfAllPhysicalSpellSkillGems = 3;
        }

        public override void AddLevel(Gem[] gems)
        {
            foreach(var gem in gems.Where(i=>i.Tags.Any(j=>j==Tag.Physical && j == Tag.Spell) && i is SkillGem)) //get only skill gems with “physical” and “spell”
            {
                gem.AddLevelReseting(3);
            }
        }
        public override void AddEffect(SkillGem skill)
        {
            if (skill.Tags.Any(i => i == Tag.Spell))
            {
                var damage = skill.Damage.Find(x => x.Type == DamageType.physical);
                if (damage != null)
                    damage.Amount += 27;
                else
                    skill.Damage.Add(
                        new Damage
                        {
                            Type = DamageType.physical, Amount = 27
                        });
            }

            foreach (var item in skill.Damage)
            {
                if (item.Type == DamageType.lightning || item.Type == DamageType.cold || item.Type == DamageType.fire)
                {
                    item.Amount = 0;
                }
            }
        }
    }
}
