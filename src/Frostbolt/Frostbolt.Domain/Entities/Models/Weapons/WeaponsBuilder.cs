﻿namespace Frostbolt.Domain.Entities
{
    public class WeaponsBuilder
    {
        private readonly string _nameEquip;

        public WeaponsBuilder(string nameEquip)
        {
            _nameEquip = nameEquip;
        }

        public Weapon Build()
        {
            if (_nameEquip == "cold_iron_point")
            {
                return new ColdIronPoint();
            }
            else if (_nameEquip == "dread_bane")
            {
                return new DreadBane();
            }
            else if (_nameEquip == "pledge_of_hands")
            {
                return new PledgeOfHands();
            }
            else
            {
                return null;
            }

        }

    }
}
