﻿namespace Frostbolt.Domain.Entities
{
    public class DivineLifeFlask : Flask
    {
        public DivineLifeFlask()
        {
            Name = "Divine Life Flask";
            Power = 2400;
        }
    }
}
