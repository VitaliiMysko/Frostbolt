﻿namespace Frostbolt.Domain.Entities
{
    public class GiantLifeFlask : Flask
    {
        public GiantLifeFlask()
        {
            Name = "Giant Life flask";
            Power = 830;
        }
    }
}
