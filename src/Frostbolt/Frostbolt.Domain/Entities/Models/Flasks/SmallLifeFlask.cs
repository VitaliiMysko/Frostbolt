﻿namespace Frostbolt.Domain.Entities
{
    public class SmallLifeFlask : Flask
    {
        public SmallLifeFlask()
        {
            Name = "Small Life Flask";
            Power = 70;
        }
    }
}
