﻿namespace Frostbolt.Domain.Entities
{
    public class FlasksBuilder
    {
        private readonly string _nameEquip;

        public FlasksBuilder(string nameEquip)
        {
            _nameEquip = nameEquip;
        }

        public Flask Build()
        {
            if (_nameEquip == "divine_life_flask")
            {
                return new DivineLifeFlask();
            }
            else if (_nameEquip == "giant_life_flask")
            {
                return new GiantLifeFlask();
            }
            else if (_nameEquip == "small_life_flask")
            {
                return new SmallLifeFlask();
            }
            else
            {
                return null;
            }

        }


    }
}
