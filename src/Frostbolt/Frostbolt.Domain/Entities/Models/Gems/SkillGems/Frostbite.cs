﻿using Frostbolt.Domain.Interfaces;
using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frostbolt.Domain.Entities
{
    public class Frostbite : SkillGem, IMergeableGem, ICurseGem
    {
        public ICurse Curse { get; private set; }
        public Frostbite(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Frostbite";

            Tags.AddRange(new List<Tag> { Tag.Spell, Tag.AoE, Tag.Duration, Tag.Cold, Tag.Curse, Tag.Hex });

            Color = Color.blue;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter { Name = "cold vulnerability", Count = 30 + lvl },
                new Parameter { Name = "mana cost", Count = 25 + lvl },
                new Parameter{Name = "casts", Count = 1 }
            });
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new Frostbite(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
            this.Damage = NewGem.Damage;
        }
        public override void SetIsActive(Gem[] gems, Hero hero)
        {
            if(gems.Any(i=>i is HextouchSupport))
            {
                IsActive = false;
            }
        }
        public bool IsMergeable()
        {
            if(IsActive)
            {
                return false;
            }
            return true;
        }

        public void Merge(SkillGem skillGem)
        {
            if (!Curse.IsCursable(skillGem))
            {
                return;
            }

            foreach(var thisDamage in Damage)
            {
                if(skillGem.Damage.Any(i=>i.Type == thisDamage.Type))
                {
                    var skillDamage = skillGem.Damage.Find(i => i.Type == thisDamage.Type);
                }
                else
                {
                    skillGem.Damage.Add(thisDamage);
                }
            }

            foreach (var thisParameter in Parameters)
            {
                if (skillGem.Parameters.Any(i => i.Name == thisParameter.Name))
                {
                    skillGem.Parameters.Find(i => i.Name == thisParameter.Name).Count += thisParameter.Count;
                }
                else
                {
                    skillGem.Parameters.Add(thisParameter);
                }
            }
        }

        public void SetCurse(ICurse curse)
        {
            Curse = curse;
        }
    }
}
