﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;

namespace Frostbolt.Domain.Entities
{
    public class SeismicTrap : SkillGem
    {
        public SeismicTrap(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Seismic Trap";

            Tags.AddRange(new List<Tag> { Tag.Trap, Tag.Spell, Tag.Duration, Tag.AoE, Tag.Physical });

            Color = Color.green;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "trap last second", Count = 4 },
                new Parameter{Name = "releases waves", Count = 5},
                new Parameter{Name = "casts", Count = 1 },
                new Parameter{Name = "mana cost", Count = 1 }
            });

            //--14.02.22
            Damage.Add(
                new Damage
                {
                    Type = DamageType.physical,
                    Amount = (int)Math.Round((150 + Math.Pow(lvl, 2.5)) * (1 + (double)quality / 100))
                });
        }
        public override void AddLevelReseting(int count)
        {
            var NewGem = new SeismicTrap(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
            this.Damage = NewGem.Damage;
        }
    }
}
