﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;

namespace Frostbolt.Domain.Entities
{
    public class IceNova : SkillGem
    {
        public IceNova(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Ice Nova";

            Tags.AddRange(new List<Tag> { Tag.Spell, Tag.AoE, Tag.Cold, Tag.Nova });

            Color = Color.blue;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "casts", Count = 1 },
                new Parameter{Name = "cast speed", Count = 0.7},
                new Parameter{Name = "mana cost", Count = Math.Round(8 + (double)lvl/2, 2)}
            });

            //--14.02.222
            Damage.Add( 
                new Damage 
                {   Type = DamageType.cold, 
                    Amount = (int)Math.Round((50 + 4 * Math.Pow(lvl, 2)) * (1 + (double)quality /100)) 
                });
        }
        public override void AddLevelReseting(int count)
        {
            var NewGem = new IceNova(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
            this.Damage = NewGem.Damage;
        }
    }
}
