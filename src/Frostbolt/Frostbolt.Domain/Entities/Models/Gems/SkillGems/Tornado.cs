﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;

namespace Frostbolt.Domain.Entities
{
    public class Tornado : SkillGem
    {
        public Tornado(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Tornado";

            Tags.AddRange(new List<Tag> { Tag.Spell, Tag.Duration, Tag.Physical, Tag.AoE, Tag.Orb });

            Color = Color.green;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "cast speed", Count = 0.75 },
                new Parameter{Name = "move speed", Count = Math.Round((lvl-1) * 0.03 * (1 + (double)quality/200), 2)},
                new Parameter{Name = "mana cost", Count = Math.Round(14 + (double)lvl/2, 2)},
                new Parameter{Name = "casts", Count = 1 }
            });

            //--14.02.22
            Damage.Add(
                new Damage
                {
                    Type = DamageType.physical,
                    Amount = (int)(30 + lvl * 5)
                });
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new Tornado(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
            this.Damage = NewGem.Damage;
        }
    }
}
