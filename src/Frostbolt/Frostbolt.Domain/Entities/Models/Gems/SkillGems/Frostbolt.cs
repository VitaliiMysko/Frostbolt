﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;

namespace Frostbolt.Domain.Entities
{
    public class Frostbolt : SkillGem
    {
        public Frostbolt(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Frostbolt";

            Tags.AddRange(new List<Tag> { Tag.Spell, Tag.Projectile, Tag.Cold });

            Color = Color.blue;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "piercing", Count = 0 },
                new Parameter { Name = "projectiles", Count = 1 },
                new Parameter { Name = "cast speed", Count = 0.75 },
                new Parameter { Name = "mana cost", Count = 5 + lvl },
                new Parameter{Name = "casts", Count = 1 }
            });

            //--14.02.22
            Damage.Add( 
                new Damage 
                {   
                    Type = DamageType.cold, 
                    Amount = (int)Math.Round((200 + 5 * Math.Pow(lvl, 2)) * (1 + (double)quality / 100)) 
                });
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new Frostbolt(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
            this.Damage = NewGem.Damage;
        }
    }
}
