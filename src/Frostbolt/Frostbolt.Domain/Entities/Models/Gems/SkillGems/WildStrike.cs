﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;
using System.Text;

namespace Frostbolt.Domain.Entities
{
    public class WildStrike : SkillGem
    {
        public WildStrike(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Wild Strike";

            Tags.AddRange(new List<Tag> { Tag.Attack, Tag.Melee, Tag.Strike, Tag.Lightning, Tag.Cold, Tag.Fire, Tag.Projectile, Tag.AoE, Tag.Chaining, Tag.Prismatic });

            Color = Color.green;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "piercing", Count = 0 },
                new Parameter{Name = "projectiles", Count = 3},

                new Parameter{Name = "attack speed", Count = 1},
                new Parameter{Name = "mana cost", Count = 6},
                new Parameter{Name = "projectile speed", Count = 1800},
                new Parameter{Name = "casts", Count = 1 }
            });

            ElementalDamage damage = new()
            {
                Type = DamageType.elemental,
                Amount = (int)Math.Round((250 + 8 * Math.Pow(lvl, 3)) * (1 + (double)quality / 100))
            };
            damage.DamageTypesCycle.Enqueue(DamageType.fire);
            damage.DamageTypesCycle.Enqueue(DamageType.lightning);
            damage.DamageTypesCycle.Enqueue(DamageType.cold);
            //--14.02.22
            Damage.Add(damage);

            //{ DamageType.fire, DamageType.lightning, DamageType.cold }
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new WildStrike(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
            this.Damage = NewGem.Damage;
        }

        public override void SetIsActive(Gem[] gems, Hero hero)
        {
            if (hero.Weapon != null)
                IsActive = true;
            else
                IsActive = false;
        }
    }
}
