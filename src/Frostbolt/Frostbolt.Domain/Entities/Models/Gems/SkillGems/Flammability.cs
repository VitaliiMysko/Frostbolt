﻿using Frostbolt.Domain.Interfaces;
using Frostbolt.Domain.Sets;
using System.Collections.Generic;
using System.Linq;

namespace Frostbolt.Domain.Entities
{
    public class Flammability : SkillGem, IMergeableGem, ICurseGem
    {
        public ICurse Curse { get; private set; }
        public Flammability(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Flammability";

            Tags.AddRange(new List<Tag> { Tag.Spell, Tag.AoE, Tag.Duration, Tag.Fire, Tag.Curse, Tag.Hex });

            Color = Color.blue;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter { Name = "fire vulnerability", Count = 30 + lvl },
                new Parameter { Name = "mana cost", Count = 25 + lvl },
                new Parameter{Name = "casts", Count = 1 }
            });
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new Flammability(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
            this.Damage = NewGem.Damage;
        }
        public override void SetIsActive(Gem[] gems, Hero hero)
        {
            if (gems.Any(i => i is HextouchSupport))
            {
                IsActive = false;
            }
        }
        public bool IsMergeable()
        {
            if (IsActive)
            {
                return false;
            }
            return true;
        }

        public void Merge(SkillGem skillGem)
        {
            if (!Curse.IsCursable(skillGem))
            {
                return;
            }

            foreach (var thisDamage in Damage)
            {
                if (skillGem.Damage.Any(i => i.Type == thisDamage.Type))
                {
                    var skillDamage = skillGem.Damage.Find(i => i.Type == thisDamage.Type);
                }
                else
                {
                    skillGem.Damage.Add(thisDamage);
                }
            }

            foreach (var thisParameter in Parameters)
            {
                if (skillGem.Parameters.Any(i => i.Name == thisParameter.Name))
                {
                    skillGem.Parameters.Find(i => i.Name == thisParameter.Name).Count += thisParameter.Count;
                }
                else
                {
                    skillGem.Parameters.Add(thisParameter);
                }
            }
        }

        public void SetCurse(ICurse curse)
        {
            Curse = curse;
        }
    }
}