﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Frostbolt.Domain.Entities
{
    public class PoisonousConcoction : SkillGem
    {
        public int flaskPower;
        public PoisonousConcoction(int lvl, int flaskPower = 0)
        {
            this.lvl = lvl;
            this.quality = 20;
            Name = "Poisonous Concoction";

            Tags.AddRange(new List<Tag> { Tag.Attack, Tag.AoE, Tag.Chaos, Tag.Projectile });

            Color = Color.green;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "projectiles", Count = 1 },
                new Parameter{Name = "attack speed", Count = 1.5},
                new Parameter{Name = "mana cost", Count = Math.Round(6 + (double)lvl/7, 2)},
                new Parameter{Name = "casts", Count = 1 }
            });

            //--14.02.22
            Damage.Add(
                new Damage 
                {   
                    Type = DamageType.chaos, 
                    Amount = (int)Math.Round((45 + Math.Pow(lvl, 2.5) * 0.5) + flaskPower * (2 + (double)lvl /3) / 20) 
                });
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new PoisonousConcoction(lvl + count, flaskPower);
            this.lvl = lvl + count;
            this.Parameters = NewGem.Parameters;
            this.Damage = NewGem.Damage;
        }

        public override void SetIsActive(Gem[] gems, Hero hero)
        {
            if (hero.Weapon == null && hero.Flasks.ToList().Any(i=>i!=null))
                IsActive = true;
            else
                IsActive = false;
        }
    }
}
