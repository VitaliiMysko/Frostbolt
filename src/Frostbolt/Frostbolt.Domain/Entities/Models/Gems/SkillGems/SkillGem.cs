﻿using System.Collections.Generic;

namespace Frostbolt.Domain.Entities
{
    public abstract class SkillGem : Gem
    {
        public bool IsActive = true;
        public List<Damage> BaseDamage { set; get; } = new();
        public List<Damage> Damage = new();

        public virtual void SetIsActive(Gem[] gems, Hero hero) { }

        public override object Clone()
        {
            SkillGem cloneGem = (SkillGem)base.Clone();

            cloneGem.Damage = new();
            foreach (var damage in Damage)
            {
                cloneGem.Damage.Add((Damage)damage.Clone());
            }

            return cloneGem;
        }
    }
}
