﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;

namespace Frostbolt.Domain.Entities
{
    public class GreaterMultipleProjectilesSupport : SupportGem
    {
        public GreaterMultipleProjectilesSupport(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Greater Multiple Projectiles Support";

            Tags.AddRange(new List<Tag> { Tag.Support, Tag.Projectile });

            Color = Color.green;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "projectiles add", Count = 4 },
                new Parameter{Name = "angles: spread by [-10, -5, 0, 5, 10]", Count = 0},
                new Parameter{Name = "mana multiplier", Count = 1.5},
                new Parameter{Name = "all damage reduced by", Count = Math.Round((35 - (double)lvl/10 - (double)quality/2)/100, 2)}
            });
        }
        public override void AddEffect(SkillGem skillGem)
        {
            skillGem.Parameters.Find(i => i.Name == "projectiles").Count += Parameters[0].Count;
            skillGem.Parameters.Add(Parameters[1]);
            skillGem.Parameters.Find(i => i.Name == "mana cost").Count *= Parameters[2].Count;
            var factor = 1 - Parameters[3].Count;

            foreach(var item in skillGem.Damage)
                item.Amount = (int)(item.Amount * (1 - (double)factor / 100));
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new GreaterMultipleProjectilesSupport(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
        }
    }
}
