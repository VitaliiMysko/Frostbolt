﻿using Frostbolt.Domain.Interfaces;
using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frostbolt.Domain.Entities
{
    public class HextouchSupport : SupportGem
    {
        public HextouchSupport(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Hextouch Support";

            Tags.AddRange(new List<Tag> { Tag.Support, Tag.Hex });

            Color = Color.blue;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "mana multiplier", Count = 1.4}
            });
        }
        public override void AddEffect(SkillGem skillGem)
        {
            if (!skillGem.Tags.Any(i => i == Tag.Curse))
            {
                return;
            }

            skillGem.IsActive = false;

            var curseGem = (ICurseGem)skillGem;
            curseGem.SetCurse(new ProjectileCurse());
            skillGem.Parameters.Find(i => i.Name == "mana cost").Count *= Parameters[0].Count;
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new HextouchSupport(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
        }
    }
}
