﻿namespace Frostbolt.Domain.Entities
{
    public abstract class SupportGem : Gem
    {
        abstract public void AddEffect(SkillGem skillGem);
    }
}
