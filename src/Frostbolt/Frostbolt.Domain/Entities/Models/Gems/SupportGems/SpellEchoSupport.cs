﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;

namespace Frostbolt.Domain.Entities
{
    public class SpellEchoSupport : SupportGem
    {
        public SpellEchoSupport(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Spell Echo Support";

            Tags.AddRange(new List<Tag> { Tag.Spell, Tag.Support });

            Color = Color.blue;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "any spell instances (casts, projectiles...): repeat additional time", Count = 0 },
                new Parameter{Name = "mana multiplier", Count = 1.5},
                new Parameter{Name = "all damage multiplier", Count = Math.Round(0.9 * (1 + (double)quality/200), 2)},
                new Parameter{Name = "cast speed", Count = Math.Round(0.54 - (0.01 * lvl), 2)}
            });
        }

        public override void AddEffect(SkillGem skillGem)
        {
            skillGem.Parameters.Add(Parameters[0]);
            skillGem.Parameters.Find(i => i.Name == "mana cost").Count *= Parameters[1].Count;

            //--14.02.22
            foreach (var item in skillGem.Damage)
            { 
                item.Amount =(int)(item.Amount * Parameters[2].Count);
            }

            skillGem.Parameters.Find(i => i.Name == "cast speed").Count *= Parameters[3].Count;
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new SpellEchoSupport(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
        }
    }
}
