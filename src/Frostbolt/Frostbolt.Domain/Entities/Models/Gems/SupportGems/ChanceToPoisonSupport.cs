﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;

namespace Frostbolt.Domain.Entities
{
    public class ChanceToPoisonSupport : SupportGem
    {
        public ChanceToPoisonSupport(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Chance to Poison Support";

            Tags.AddRange(new List<Tag> { Tag.Support, Tag.Chaos });

            Color = Color.green;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "chance to poison on hit", Count = 0.4 },
                new Parameter {Name = "chaos damage", Count = Math.Round(5*lvl*(1+(double)quality/200), 2)},
                new Parameter{Name = "mana multiplier", Count = 1.2 }
            });
        }
        public override void AddEffect(SkillGem skillGem)
        {
            skillGem.Parameters.Add(Parameters[0]);

            //--14.02.22
            foreach (var item in skillGem.Damage)
            { 
                if (item.Type == DamageType.chaos)
                    item.Amount += (int)(item.Amount + Parameters[1].Count);
                else
                    item.Amount += item.Amount;
            }

            skillGem.Parameters.Find(i => i.Name == "mana cost").Count *= Parameters[2].Count;
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new ChanceToPoisonSupport(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
        }
    }
}
