﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;

namespace Frostbolt.Domain.Entities
{
    public class SpellCascadeSupport : SupportGem
    {
        public SpellCascadeSupport(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Spell Cascade Support";

            Tags.AddRange(new List<Tag> { Tag.AoE, Tag.Spell, Tag.Support });

            Color = Color.blue;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "mana multiplier", Count = 1.3},
                new Parameter{Name = "add “apply to front and back”", Count = 0 },
                new Parameter{Name = "all damage multiplier", Count = Math.Round((0.6 + 0.01 * lvl) * (1 + (double)quality/200), 2)}
            });
        }
        public override void AddEffect(SkillGem skillGem)
        {
            skillGem.Parameters.Find(i => i.Name == "mana cost").Count *= Parameters[0].Count;
            skillGem.Parameters.Add(Parameters[1]);

            //--14.02.22
            foreach(var item in skillGem.Damage)
                item.Amount *= (int)Parameters[2].Count;
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new SpellCascadeSupport(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
        }
    }
}
