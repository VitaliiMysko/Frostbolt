﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Frostbolt.Domain.Entities
{
    public class AddedLightningDamageSupport : SupportGem
    {
        public AddedLightningDamageSupport(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Added Lightning Damage Support";

            Tags.AddRange(new List<Tag> { Tag.Support });

            Color = Color.blue;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "lightning damage", Count = Math.Round(Math.Pow(lvl, 2) * (1 + (double)quality/200), 2) },
                new Parameter { Name = "mana multiplier", Count = 1.2 },
            });
        }
        public override void AddEffect(SkillGem skillGem)
        {
            if (skillGem.Damage.Any(i => i.Type == DamageType.lightning))
            {
                skillGem.Damage.Find(i => i.Type == DamageType.lightning).Amount += (int)Parameters[0].Count; 
                skillGem.BaseDamage.Find(i => i.Type == DamageType.lightning).Amount += (int)Parameters[0].Count;
            }
            else
            {
                skillGem.Damage.Add(new Damage { Type = DamageType.lightning, Amount = (int)Parameters[0].Count });
                skillGem.BaseDamage.Add(new Damage { Type = DamageType.lightning, Amount = (int)Parameters[0].Count });
            }
            skillGem.Parameters.Find(i => i.Name == "mana cost").Count *= Parameters[1].Count;
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new AddedLightningDamageSupport(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
        }
    }
}
