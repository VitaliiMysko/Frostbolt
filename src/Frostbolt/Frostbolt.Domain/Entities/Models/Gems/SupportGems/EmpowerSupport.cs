﻿using Frostbolt.Domain.Sets;
using System;
using System.Collections.Generic;

namespace Frostbolt.Domain.Entities
{
    public class EmpowerSupport : SupportGem
    {
        public EmpowerSupport(int lvl, int quality)
        {
            this.lvl = lvl;
            this.quality = quality;
            Name = "Empower Support";

            Tags.AddRange(new List<Tag> { Tag.Support });

            Color = Color.red;

            Parameters.AddRange(new List<Parameter>
            {
                new Parameter{Name = "all linked skill gems get levels", Count = lvl/6 },
                new Parameter{Name = "mana multiplier", Count = 1.3},
            });
        }
        public override void AddEffect(SkillGem skillGem)
        {
            skillGem.AddLevelReseting((int)Parameters[0].Count);
            skillGem.Parameters.Find(i => i.Name == "mana cost").Count *= Parameters[1].Count;
            skillGem.Parameters.Find(i => i.Name == "mana cost").Count = Math.Round(skillGem.Parameters.Find(i => i.Name == "mana cost").Count, 2);
        }

        public override void AddLevelReseting(int count)
        {
            var NewGem = new EmpowerSupport(lvl + count, quality);
            this.Parameters = NewGem.Parameters;
            this.lvl = NewGem.lvl;
        }
    }
}
