﻿namespace Frostbolt.Domain.Entities
{
    public class GemsBuilder
    {
        private readonly string _nameGem;
        private readonly int _lvl;
        private readonly int _quality;

        public GemsBuilder(string nameGem, int lvl = 20, int quality = 20)
        {
            _nameGem = nameGem;
            _lvl = lvl;
            _quality = quality;
        }

        public Gem Build()
        {
            if (_nameGem == "frostbolt")
            {
                return new Frostbolt(_lvl, _quality);
            }
            else if(_nameGem == "frostbite")
            {
                return new Frostbite(_lvl, _quality);
            }
            else if(_nameGem == "flammability")
            {
                return new Flammability(_lvl, _quality);
            }
            else if (_nameGem == "ice nova")
            {
                return new IceNova(_lvl, _quality);
            }
            else if(_nameGem == "wild strike")
            {
                return new WildStrike(_lvl, _quality);
            }
            else if (_nameGem == "poisonous concoction")
            {
                var hero = Hero.GetInstance();
                return new PoisonousConcoction(_lvl, hero.GetFlasksPower());
            }
            else if (_nameGem == "seismic trap")
            {
                return new SeismicTrap(_lvl, _quality);
            }
            else if (_nameGem == "tornado")
            {
                return new Tornado(_lvl, _quality);
            }
            else if (_nameGem == "added lightning damage support")
            {
                return new AddedLightningDamageSupport(_lvl, _quality);
            }
            else if (_nameGem == "chance to poison support")
            {
                return new ChanceToPoisonSupport(_lvl, _quality);
            }
            else if(_nameGem == "hextouch support")
            {
                return new HextouchSupport(_lvl, _quality);
            }
            else if (_nameGem == "empower support")
            {
                return new EmpowerSupport(_lvl, _quality);
            }
            else if (_nameGem == "greater multiple projectiles support")
            {
                return new GreaterMultipleProjectilesSupport(_lvl, _quality);
            }
            else if (_nameGem == "spell sascade support")
            {
                return new SpellCascadeSupport(_lvl, _quality);
            }
            else if (_nameGem == "spell echo support")
            {
                return new SpellEchoSupport(_lvl, _quality);
            }
            else
            {
                return null;
            }
        }
    }
}
