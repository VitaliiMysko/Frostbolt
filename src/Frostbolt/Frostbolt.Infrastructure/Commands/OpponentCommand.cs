﻿using Frostbolt.Domain.Entities;
using Frostbolt.Domain.Interfaces;
using Frostbolt.Domain.Sets;
using System;

namespace Frostbolt.Infrastructure.Commands
{
    public class OpponentCommand : ICommand
    {
        private readonly string[] _commandDataArr;
        private readonly string _commandEntered;

        private int _life = 0;
        private int _coldResistance = 0;
        private int _fireResistance = 0;
        private int _chaosResistance = 0;
        private int _lightningResistance = 0;
        private int _physicalResistance = 0;

        public OpponentCommand(string[] dataArr)
        {
            _commandDataArr = dataArr;
            _commandEntered = nameof(Command.Opponent) + ": " + String.Join(", ", _commandDataArr);
        }

        private bool DoValidation(Report report)
        {
            if (_commandDataArr.Length > 6)
            {
                report.AddMessage("Error! Record is not correct");
                return false;
            }

            if (_commandDataArr.Length >= 1)
            {
                var lifeStr = _commandDataArr[0].ToLower().Trim();

                try
                {
                    var life = double.Parse(lifeStr);
                    _life = (int)life;
                }
                catch (Exception)
                {
                    report.AddMessage($"Error: value of life \"{lifeStr}\" is not correct");
                    return false;
                }

            }

            if (_commandDataArr.Length >= 2)
            {
                var coldResistanceSrt = _commandDataArr[1].ToLower().Trim();

                try
                {
                    var coldResistance = double.Parse(coldResistanceSrt);
                    _coldResistance = (int)coldResistance;
                }
                catch (Exception)
                {
                    report.AddMessage($"Error: value of cold resistance\"{coldResistanceSrt}\" is not correct");
                    return false;
                }
            }

            if (_commandDataArr.Length >= 3)
            {
                var fireResistanceSrt = _commandDataArr[2].ToLower().Trim();

                try
                {
                    var fireResistance = double.Parse(fireResistanceSrt);
                    _fireResistance = (int)fireResistance;
                }
                catch (Exception)
                {
                    report.AddMessage($"Error: value of fire resistance\"{fireResistanceSrt}\" is not correct");
                    return false;
                }
            }

            if (_commandDataArr.Length >= 4)
            {
                var chaosResistanceSrt = _commandDataArr[3].ToLower().Trim();

                try
                {
                    var chaosResistance = double.Parse(chaosResistanceSrt);
                    _chaosResistance = (int)chaosResistance;
                }
                catch (Exception)
                {
                    report.AddMessage($"Error: value of chaos resistance\"{chaosResistanceSrt}\" is not correct");
                    return false;
                }
            }

            if (_commandDataArr.Length >= 5)
            {
                var lightningResistanceSrt = _commandDataArr[4].ToLower().Trim();

                try
                {
                    var lightningResistance = double.Parse(lightningResistanceSrt);
                    _lightningResistance = (int)lightningResistance;
                }
                catch (Exception)
                {
                    report.AddMessage($"Error: value of lightning resistance\"{lightningResistanceSrt}\" is not correct");
                    return false;
                }
            }

            if (_commandDataArr.Length >= 6)
            {
                var physicalResistanceSrt = _commandDataArr[5].ToLower().Trim();

                try
                {
                    var physicalResistance = double.Parse(physicalResistanceSrt);
                    _physicalResistance = (int)physicalResistance;
                }
                catch (Exception)
                {
                    report.AddMessage($"Error: value of physical resistance\"{physicalResistanceSrt}\" is not correct");
                    return false;
                }
            }

            return true;
        }

        public Report Execute()
        {
            var report = new Report();
            report.Command = Command.Opponent;
            report.CommandEntered = _commandEntered;

            if (!DoValidation(report))
                return report;

            SetOpponent(report, _life, _coldResistance, _fireResistance, _chaosResistance, _lightningResistance, _physicalResistance);

            return report;
        }

        private void SetOpponent(Report report, int life, int coldResistance, int fireResistance, int chaosResistance, int lightningResistance, int physicalResistance)
        {
            var opponent = Opponent.GetInstance();
            
            opponent.Life = life;
            opponent.ColdResistance = coldResistance;
            opponent.FireResistance = fireResistance;
            opponent.ChaosResistance = chaosResistance;
            opponent.LightningResistance = lightningResistance;
            opponent.PhysicalResistance = physicalResistance;
            opponent.Parameters = new();
        }
    }
}
