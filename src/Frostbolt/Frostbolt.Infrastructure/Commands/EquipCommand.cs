﻿using Frostbolt.Domain.Entities;
using Frostbolt.Domain.Interfaces;
using Frostbolt.Domain.Sets;
using System;

namespace Frostbolt.Infrastructure.Commands
{
    public class EquipCommand : ICommand
    {
        private readonly string[] _commandDataArr;
        private readonly string _commandEntered;

        public EquipCommand(string[] dataArr)
        {
            _commandDataArr = dataArr;
            _commandEntered = nameof(Command.Equip) + ": " + String.Join(", ", _commandDataArr);
        }

        private bool DoValidation(Report report)
        {
            if (_commandDataArr.Length != 2)
            {
                report.AddMessage("Error! Record is not correct");
                return false;
            }

            return true;
        }

        public Report Execute()
        {
            var report = new Report();
            report.Command = Command.Equip;
            report.CommandEntered = _commandEntered;

            if (!DoValidation(report))
                return report;

            var typeEquip = _commandDataArr[0].ToLower().Trim();
            var nameEquip = _commandDataArr[1].ToLower().Trim();

            EquipTheHero(report, typeEquip, nameEquip);

            return report;
        }

        private void EquipTheHero(Report report, string typeEquip, string nameEquip)
        {
            var hero = Hero.GetInstance();

            if (typeEquip == "body")
            {
                hero.BodyArmour = GetBody(report, nameEquip);
            }
            else if (typeEquip == "weapon")
            {
                hero.Weapon = GetWeapon(report, nameEquip);
            }
            else if (typeEquip == "flask")
            {
                hero.Flasks = GetFlasks(report, nameEquip, hero.Flasks);
            }
            else
            {
                report.AddMessage($"Error! {typeEquip} - Unknown typy of equip");
            }
        }

        private Flask[] GetFlasks(Report report, string nameEquip, Flask[] heroFlasks)
        {
            var newFlask = new FlasksBuilder(nameEquip).Build();

            if (newFlask == null)
            {
                report.AddMessage($"Error! {nameEquip} - Unknown flask");
                return heroFlasks;
            }

            for (int i = 0; i < heroFlasks.Length; i++)
            {
                if (heroFlasks[i] == null)
                {
                    heroFlasks[i] = newFlask;
                    break;
                }
            }

            report.CommandSucceeded = true;
            return heroFlasks;
        }

        private Weapon GetWeapon(Report report, string nameEquip)
        {
            var weapon = new WeaponsBuilder(nameEquip).Build();

            if (weapon == null)
                report.AddMessage($"Error! {nameEquip} - Unknown weapon equip");

            report.CommandSucceeded = true;
            return weapon;
        }

        private BodyArmour GetBody(Report report, string nameEquip)
        {
            var bodyArmour = new BodiesBuilder(nameEquip).Build();

            if (bodyArmour == null)
                report.AddMessage($"Error! {nameEquip} - Unknown body equip");

            report.CommandSucceeded = true;
            return bodyArmour;
        }
    }
}
