﻿using Frostbolt.Domain.Entities;
using Frostbolt.Domain.Interfaces;
using Frostbolt.Domain.Sets;
using System;

namespace Frostbolt.Infrastructure.Commands
{
    public class SetbuttonCommand : ICommand
    {
        private readonly string[] _commandDataArr;
        private readonly string _commandEntered;

        private int _lvl = 20;
        private int _quality = 20;

        public SetbuttonCommand(string[] dataArr)
        {
            _commandDataArr = dataArr;
            _commandEntered = nameof(Command.Equip) + ": " + String.Join(", ", _commandDataArr);
        }

        private bool DoValidation(Report report)
        {
            if (_commandDataArr.Length < 3 && _commandDataArr.Length > 5)
            {
                report.AddMessage("Error! Record is not correct");
                return false;
            }

            if (_commandDataArr.Length > 3)
            {
                var lvlStr = _commandDataArr[3].ToLower().Trim();

                try
                {
                    var lvl = double.Parse(lvlStr);
                    _lvl = (int)lvl;
                }
                catch (Exception)
                {
                    report.AddMessage($"Error: number of slot \"{lvlStr}\" is not correct");
                    return false;
                }
            }

            if (_commandDataArr.Length > 4)
            {
                var qualitySrt = _commandDataArr[4].ToLower().Trim();

                try
                {
                    var quality = double.Parse(qualitySrt);
                    _quality = (int)quality;
                }
                catch (Exception)
                {
                    report.AddMessage($"Error: number of slot \"{qualitySrt}\" is not correct");
                    return false;
                }
            }

            return true;
        }

        public Report Execute()
        {
            var report = new Report();
            report.Command = Command.Equip;
            report.CommandEntered = _commandEntered;

            if (!DoValidation(report))
                return report;

            var key = _commandDataArr[0].ToLower().Trim() + " " + _commandDataArr[1].ToLower().Trim();
            var nameGem = _commandDataArr[2].ToLower().Trim().Replace("_", " ");

            var gem = GetGem(report, nameGem, _lvl, _quality);

            if (gem == null)
                return report;


            if (gem.Tags.FindIndex(t => t == Tag.Support) >= 0)
            {
                report.AddMessage($"Error! Gem {nameGem} - must not be support");
                return report;
            }

            var hotBars = HotBar.HotBars;

            if (hotBars.ContainsKey(key))
            {
                hotBars.Remove(key);
            }

            var nameGemFull = nameGem + "_" + _lvl + "_" + _quality;

            hotBars.Add(key, nameGemFull);

            report.CommandSucceeded = true;
            return report;
        }


        private Gem GetGem(Report report, string nameGem, int lvl, int quality)
        {
            var gem = new GemsBuilder(nameGem, lvl, quality).Build();

            if (gem == null)
                report.AddMessage($"Error! {nameGem} - Unknown gem");

            report.CommandSucceeded = true;
            return gem;
        }
    }
}
