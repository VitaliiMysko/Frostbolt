﻿using Frostbolt.Domain.Interfaces;
using Frostbolt.Domain.Sets;
using System;
using System.Linq;

namespace Frostbolt.Infrastructure.Commands
{
    public class CommandBuilder
    {
        private readonly string _action;
        private readonly string[] _dataArr;
        public CommandBuilder(string[] dataArrFull)
        {
            _action = dataArrFull[0];
            _dataArr = dataArrFull.Where((item, idx) => idx != 0).ToArray();
        }

        public ICommand Build()
        {

            switch (_action)
            {
                case nameof(Command.Equip):

                    return new EquipCommand(_dataArr);

                case nameof(Command.Press):

                    return new PressCommand(_dataArr);

                case nameof(Command.Setbutton):

                    return new SetbuttonCommand(_dataArr);

                case nameof(Command.Setgem):

                    return new SetgemCommand(_dataArr);

                case nameof(Command.Opponent):

                    return new OpponentCommand(_dataArr);

                case nameof(Command.Characters):

                    return new CharactersCommand(_dataArr);

                default:

                    throw new Exception($"Error! Command \"{_action}\" is not correct");
            }

        }
    }
}