﻿using Frostbolt.Domain.Entities;
using Frostbolt.Domain.Interfaces;
using Frostbolt.Domain.Sets;
using System;

namespace Frostbolt.Infrastructure.Commands
{
    public class SetgemCommand : ICommand
    {
        private readonly string[] _commandDataArr;
        private readonly string _commandEntered;

        private int _numberSlot;
        private int _lvl = 20;
        private int _quality = 20;

        public SetgemCommand(string[] dataArr)
        {
            _commandDataArr = dataArr;
            _commandEntered = nameof(Command.Equip) + ": " + String.Join(", ", _commandDataArr);
        }

        private bool DoValidation(Report report)
        {
            if (_commandDataArr.Length < 2 && _commandDataArr.Length > 4)
            {
                report.AddMessage("Error! Record is not correct");
                return false;
            }

            if (_commandDataArr.Length > 1)
            {
                var numberSlotSrt = _commandDataArr[0].ToLower().Trim();

                try
                {
                    var numberSlot = double.Parse(numberSlotSrt);
                    _numberSlot = (int)numberSlot;
                }
                catch (Exception)
                {
                    report.AddMessage($"Error: number of slot \"{numberSlotSrt}\" is not correct");
                    return false;
                }

            }

            if (_commandDataArr.Length > 2)
            {
                var lvlStr = _commandDataArr[2].ToLower().Trim();

                try
                {
                    var lvl = double.Parse(lvlStr);
                    _lvl = (int)lvl;
                }
                catch (Exception)
                {
                    report.AddMessage($"Error: number of slot \"{lvlStr}\" is not correct");
                    return false;
                }
            }

            if (_commandDataArr.Length > 3)
            {
                var qualitySrt = _commandDataArr[3].ToLower().Trim();

                try
                {
                    var quality = double.Parse(qualitySrt);
                    _quality = (int)quality;
                }
                catch (Exception)
                {
                    report.AddMessage($"Error: number of slot \"{qualitySrt}\" is not correct");
                    return false;
                }
            }

            return true;
        }

        public Report Execute()
        {
            var report = new Report();
            report.Command = Command.Equip;
            report.CommandEntered = _commandEntered;

            if (!DoValidation(report))
                return report;

            var nameGem = _commandDataArr[1].ToLower().Trim().Replace("_", " ");

            SetGem(report, nameGem, _numberSlot, _lvl, _quality);

            return report;
        }

        private void SetGem(Report report, string nameGem, int numberSlot, int lvl, int quality)
        {
            var hero = Hero.GetInstance();

            if (hero.BodyArmour == null)
            {
                report.AddMessage($"Error! Hero not have body armour.");
                return;
            }

            var slots = hero.BodyArmour.Slots;

            if (numberSlot > slots.Length)
            {
                report.AddMessage($"Error: number of slot \"{numberSlot}\" is not correct");
                return;
            }

            var gem = GetGem(report, nameGem, lvl, quality);

            if (gem == null)
            {
                return;
            }

            var idx = numberSlot - 1;
            slots[idx].Gem = gem;
        }

        private Gem GetGem(Report report, string nameGem, int lvl, int quality)
        {
            var gem = new GemsBuilder(nameGem, lvl, quality).Build();

            if (gem == null)
                report.AddMessage($"Error! {nameGem} - Unknown gem");

            report.CommandSucceeded = true;
            return gem;
        }

    }
}
