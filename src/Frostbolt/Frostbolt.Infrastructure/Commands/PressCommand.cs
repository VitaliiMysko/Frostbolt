﻿using Frostbolt.Domain.Entities;
using Frostbolt.Domain.Interfaces;
using Frostbolt.Domain.Sets;
using System;
using System.Linq;

namespace Frostbolt.Infrastructure.Commands
{
    public class PressCommand : ICommand
    {
        private readonly string[] _commandDataArr;
        private readonly string _commandEntered;

        public PressCommand(string[] dataArr)
        {
            _commandDataArr = dataArr;
            _commandEntered = nameof(Command.Equip) + ": " + String.Join(", ", _commandDataArr);
        }

        private bool DoValidation(Report report)
        {
            if (_commandDataArr.Length != 2)
            {
                report.AddMessage("Error! Record is not correct");
                return false;
            }

            return true;
        }

        public Report Execute()
        {
            var report = new Report();
            report.Command = Command.Equip;
            report.CommandEntered = _commandEntered;

            if (!DoValidation(report))
                return report;


            var key = _commandDataArr[0].ToLower().Trim() + " " + _commandDataArr[1].ToLower().Trim();

            var hotBars = HotBar.HotBars;

            if (!hotBars.ContainsKey(key))
            {
                report.AddMessage($"Error! {key} - not found");
                return report;
            }

            var gemRun = hotBars[key].Split("_");

            var gemName = gemRun[0];
            var lvlStr = gemRun[1];
            var qualitySrt = gemRun[2];

            double lvl;

            try
            {
                lvl = double.Parse(lvlStr);

            }
            catch (Exception)
            {
                report.AddMessage($"Error: cannot write save command");
                return report;
            }


            double quality;

            try
            {
                quality = double.Parse(qualitySrt);
            }
            catch (Exception)
            {
                report.AddMessage($"Error: cannot write save command");
                return report;
            }

            return Run(gemName, lvl, quality);
        }

        private static Report Run(string gemName, double lvl, double quality)
        {
            var report = new Report();
            var hero = Hero.GetInstance();

            var slots = hero.BodyArmour.Slots;

            var slot = slots.ToList().Find(slot => slot.Gem != null &&
                                            slot.Gem.Name.ToLower() == gemName &&
                                            slot.Gem.lvl == (int)lvl &&
                                            slot.Gem.quality == (int)quality);

            if (slot == null)
            {
                report.AddMessage("Gem not found");
                report.CommandSucceeded = false;
                return report;
            }

            int skillIndex = 0;
            foreach (var item in slots)
            {
                if (item == slot)
                {
                    break;
                }
                else if (!(item.Gem is SkillGem))
                {
                    continue;
                }
                else skillIndex++;
            }

            var skill = hero.GetSkills()[skillIndex];
            report.AddMessage(GetSkillMessage(skill));

            var opponent = Opponent.GetInstance();
            int startHP = opponent.Life;

            hero.Attack(skill, opponent);

            int damage = startHP - opponent.Life;

            report.AddMessage($"\nPassed damage: {damage}");

            return report;
        }

        private static string GetSkillMessage(SkillGem skill)
        {
            string result = "";


            int sum = 0;
            foreach(var damage in skill.Damage)
            {
                result += $"{damage.Type} {damage.Amount}; ";
                sum += damage.Amount;
            }

            result += $"Total damage: {sum}; ";

            foreach (var parameter in skill.Parameters)
            {
                result += $"{parameter.Name} {parameter.Count}; ";
            }

            return result;
        }
    }
}
