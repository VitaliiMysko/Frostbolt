﻿using Frostbolt.Domain.Entities;
using Frostbolt.Domain.Interfaces;
using Frostbolt.Domain.Sets;
using System;

namespace Frostbolt.Infrastructure.Commands
{
    public class CharactersCommand : ICommand
    {
        private readonly string[] _commandDataArr;
        private readonly string _commandEntered;

        public CharactersCommand(string[] dataArr)
        {
            _commandDataArr = dataArr;
            _commandEntered = nameof(Command.Characters) + ": " + String.Join(", ", _commandDataArr);
        }

        private bool DoValidation(Report report)
        {
            if (_commandDataArr.Length > 1)
            {
                report.AddMessage("Error! Record is not correct");
                return false;
            }

            return true;
        }

        public Report Execute()
        {
            var report = new Report();
            report.Command = Command.Characters;
            report.CommandEntered = _commandEntered;

            if (!DoValidation(report))
                return report;

            report.AddMessage(Hero.GetInstance().GetDescription());
            report.AddMessage("\n");
            report.AddMessage(Opponent.GetInstance().GetDescription());

            return report;
        }

    }
}
