﻿using Frostbolt.Domain.Interfaces;
using System;

namespace Frostbolt.Infrastructure.Outputs
{
    public class OutputApp : IOutput
    {
        public void Print(string message)
        {
            Console.WriteLine(message);
        }
    }
}
