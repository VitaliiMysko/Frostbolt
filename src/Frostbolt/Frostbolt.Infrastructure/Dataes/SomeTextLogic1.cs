﻿using Frostbolt.Domain.Entities;
using Frostbolt.Domain.Sets;
using System.Collections.Generic;

namespace Frostbolt.Infrastructure.Dataes
{
    public class SomeTextLogic1
    {

        public void RunGettingSkillGroups()
        {
            //My test example
            Gem[] links = new Gem[6];

            links[0] = new PoisonousConcoction(1, 1);
            links[1] = new IceNova(1, 1);
            links[2] = new Domain.Entities.Frostbolt(1, 1);
            links[3] = new EmpowerSupport(1, 1);
            links[4] = new SpellCascadeSupport(1, 1);
            links[5] = new GreaterMultipleProjectilesSupport(1, 1);

            var gemLinks = GetSkillGroups(links);
        }

        private List<Gem[]> GetSkillGroups(Gem[] links)
        {
            List<Gem> skillLinks = new List<Gem>();
            List<Gem> supportLinks = new List<Gem>();

            foreach (var link in links)
            {
                if (link == null)
                    continue;

                if (link.Tags.FindIndex(t => t == Tag.Support) < 0)
                {
                    skillLinks.Add(link);
                    continue;
                }
                supportLinks.Add(link);
            }


            List<Gem[]> skillGroups = new List<Gem[]>();

            foreach (var skillLink in skillLinks)
            {
                List<Gem> gemLink = new List<Gem>() { skillLink };

                var skillTags = skillLink.Tags;

                foreach (var supportLink in supportLinks)
                {
                    var isLink = true;

                    foreach (var supportTag in supportLink.Tags)
                    {
                        if (supportTag == Tag.Support)
                            continue;

                        if (skillTags.FindIndex(t => t == supportTag) < 0)
                        {
                            isLink = false;
                            break;
                        }
                    }

                    if (isLink)
                    {
                        gemLink.Add(supportLink);
                    }

                }

                skillGroups.Add(gemLink.ToArray());
            }

            return skillGroups;
        }

    }
}
