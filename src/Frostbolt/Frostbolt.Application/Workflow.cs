﻿using Frostbolt.Domain.Entities;
using Frostbolt.Domain.Interfaces;
using Frostbolt.Infrastructure.Commands;
using Frostbolt.Infrastructure.Outputs;
using System;

namespace Frostbolt.Application
{
    public class Workflow
    {
        private readonly IOutput _outputApp;

        public Workflow()
        {
            _outputApp = new OutputApp();
        }

        public string[] GetData()
        {
            string actionFull = Console.ReadLine().Trim();

            if (string.IsNullOrEmpty(actionFull))
            {
                return null;
            }

            var dataArr = Helper.ParseActionLine(actionFull);
            dataArr[0] = Helper.FirstCharToUpper(dataArr[0]);

            return dataArr;
        }

        public void Run()
        {
            //===================================equip
            // equip body tabula_rasa
            // equip body corruption_sanctuary
            // equip body dendrobate_changed

            // equip weapon cold_iron_point
            // equip weapon dread_bane
            // equip weapon pledge_of_hands

            // equip flask divine_life_flask
            // equip flask giant_life_flask
            // equip flask small_life_flask

            //===================================setgem
            // setgem 1 frostbolt 5 9
            // setgem 2 ice_nova 15 5
            // setgem 3 poisonous_concoction 9 1
            // setgem 4 seismic_trap 11 22
            // setgem 5 tornado 5 

            // setgem 1 added_lightning_damage_support 5 8 
            // setgem 2 chance_to_poison_support 9
            // setgem 3 empower_support
            // setgem 4 greater_multiple_projectiles_support
            // setgem 5 spell_sascade_support
            // setgem 6 spell_echo_support

            //===================================setbutton
            // setbutton mouse right frostbolt
            // setbutton key t frostbolt 5 9
            // setbutton key q ice_nova

            //===================================press
            // press key t
            // press mouse right
            // press key q

            //===================================opponent
            //opponent 5000 20 10 0 0 50      //Life Cold Fire Chaos Lightning Physical

            //===================================characters
            //characters


            var dataArr = GetData();     // equip body tabula_rasa      //for example

            if (dataArr == null)
            {
                ToDisplay("Have you a nice day! Be.");
                return;
            }

            try
            {
                RunCommand(dataArr);

                var hero = Hero.GetInstance();
                var opponent = Opponent.GetInstance();
            }
            catch (Exception e)
            {
                ToDisplay(e.Message);
            }

            ToDisplay("");

            Run();
        }

        private void RunCommand(string[] dataArr)
        {
            var commandBuilder = new CommandBuilder(dataArr);
            var command = commandBuilder.Build();
            Report report = command.Execute();

            ShowResult(report);

        }

        private void ShowResult(Report report)
        {
            ToDisplay(report.Result);

        }

        private void ToDisplay(string message)
        {
            _outputApp.Print(message);
        }
    }
}
