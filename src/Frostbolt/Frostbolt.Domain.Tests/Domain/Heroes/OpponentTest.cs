﻿using Frostbolt.Domain.Entities;
using NUnit.Framework;

namespace Frostbolt.Tests.Domain
{
    public class OpponentTest
    {

        [TestCase(40, 2360)]
        [TestCase(20, 1700)]
        [TestCase(0, 1040)]
        public void GetHit_CheckCalculatingLife_PositiveResult(int expectedResistance, int expectedLife)
        {
            //given
            //Frostbolt Damage = 3300
            var skillGem = new Frostbolt.Domain.Entities.Frostbolt(20, 50);
            skillGem.Parameters.Add(new Parameter { Name = "cold vulnerability", Count = 20 });

            //when
            var opponent = new Opponent { Life = 5000, ColdResistance = expectedResistance };
            opponent.GetHit(skillGem);

            //then
            Assert.AreEqual(expectedLife, opponent.Life);
        }

        [Test]
        public void GetHit_HeroAttackByWildStrikeToOpponentWith80LightningResistance_PositiveResult()
        {
            //ORIGINAL conditional
            //You perform 5 key presses with a wild strike linked to Added Lightning Damage and Greater Multiple Projectiles
            //against an opponent having 80 % lightning resistance

            //convert to more simple task
            //We perform only one key presses

            //given
            var skillGemWildStrike = new WildStrike(20, 50);
            var supportGemALDS = new AddedLightningDamageSupport(15, 5);
            var supportGemGMP = new GreaterMultipleProjectilesSupport(15, 5);

            var opponent = new Opponent { Life = 5000, LightningResistance = 80 };

            var bodyArmour = new DendrobateChanged();
            bodyArmour.Slots[1].Gem = skillGemWildStrike;
            bodyArmour.Slots[3].Gem = supportGemALDS;
            bodyArmour.Slots[5].Gem = supportGemGMP;

            var hero = new Hero();
            hero.BodyArmour = bodyArmour;

            var expectedLife = 2222;                    //???

            //when
            var skill = hero.GetSkills()[0];
            opponent.GetHit(skill);

            //then
            Assert.AreEqual(expectedLife, opponent.Life);
        }

        [Test]
        public void GetHit_HeroAttackByPoisonousConcoctionWithFlaskEquippedToOpponent_PositiveResult()
        {
            //ORIGINAL conditional
            //You try to use poisonous concoction when you have flask equipped or not

            //convert to more simple task
            //You try to use poisonous concoction when you have flask equipped

            //given
            var skillGemPoisonousConcoction = new PoisonousConcoction(20, 50);

            var opponent = new Opponent { Life = 5000 };

            var bodyArmour = new DendrobateChanged();
            bodyArmour.Slots[1].Gem = skillGemPoisonousConcoction;

            var flaskSmallLifeFlask = new SmallLifeFlask();

            var hero = new Hero();
            hero.BodyArmour = bodyArmour;
            hero.Flasks[0] = flaskSmallLifeFlask;

            var expectedLife = 2222;                    //???

            //when
            var skill = hero.GetSkills()[0];
            opponent.GetHit(skill);

            //then
            Assert.AreEqual(expectedLife, opponent.Life);
        }

        [Test]
        public void GetHit_HeroAttackByPoisonousConcoctionWithoutFlaskEquippedToOpponent_NegativeResult()
        {
            //ORIGINAL conditional
            //You try to use poisonous concoction when you have flask equipped or not

            //convert to more simple task
            //You try to use poisonous concoction when you no have flask equipped

            //given
            var skillGemPoisonousConcoction = new PoisonousConcoction(20, 50);

            var opponent = new Opponent { Life = 5000 };

            var bodyArmour = new DendrobateChanged();
            bodyArmour.Slots[1].Gem = skillGemPoisonousConcoction;

            var hero = new Hero();
            hero.BodyArmour = bodyArmour;

            var expectedLife = 2222;                    //???

            //when
            var skills = hero.GetSkills();
            //opponent.GetHit(skill);


            //then
            //Assert.AreEqual(expectedLife, opponent.Life);
            Assert.IsTrue(skills.Count == 0);
        }

        [Test]
        public void GetHit_HeroAttackByFrostboltAndFrostbiteToOpponent_PositiveResult()
        {
            //You use Frostbite, Frostbolt.Then you link Frostbolt – Hextouch – Frostbite and prove that you can’t use
            //Frostbite anymore – and that the Opponent gets extra damage from Frostbite applying the effect via Frostbolt.

            //given
            var skillGemFrostbolt = new Frostbolt.Domain.Entities.Frostbolt(20, 50);
            var skillGemFrostbite = new Frostbite(20, 50);
            var supportGemHS = new HextouchSupport(15, 5);

            var opponent = new Opponent { Life = 5000 };

            var bodyArmour = new TabulaRasa();
            bodyArmour.Slots[0].Gem = skillGemFrostbite;
            bodyArmour.Slots[3].Gem = supportGemHS;
            bodyArmour.Slots[4].Gem = skillGemFrostbolt;

            var hero = new Hero();
            hero.BodyArmour = bodyArmour;

            var expectedLife = 2222;                    //???

            //when
            var skill = hero.GetSkills()[0];
            opponent.GetHit(skill);

            //then
            Assert.AreEqual(expectedLife, opponent.Life);
        }

        [Test]
        public void GetHit_HeroAttackByFrostbiteToOpponentAndThatOneGainFrostbiteEffect_PositiveResult()
        {
            //ORIGINAL conditional
            //You use Frostbite and it works on an Opponent.Then you use Flammability and Opponent loses Frostbite effect
            //and gains Flammability effect

            //convert to more simple task
            //You use Frostbite and it works on an Opponent and that one gain Frostbite effect

            //given
            var skillGemFrostbite = new Frostbite(20, 50);

            var opponent = new Opponent { Life = 5000 };

            var bodyArmour = new DendrobateChanged();
            bodyArmour.Slots[2].Gem = skillGemFrostbite;

            var hero = new Hero();
            hero.BodyArmour = bodyArmour;

            var expectedColdResistance = -20;           //???
            var expectedNameParamert = "cold vulnerability";

            //when
            var skill = hero.GetSkills()[0];
            opponent.GetHit(skill);

            //then
            Assert.AreEqual(expectedColdResistance, opponent.ColdResistance);
            Assert.AreEqual(expectedNameParamert, opponent.Parameters[0].Name);
        }


        [Test]
        public void GetHit_HeroAttackByFlammabilityToOpponentAndThatOneLostFrostbiteEffectAndGainFlammabilityEffect_PositiveResult()
        {
            //ORIGINAL conditional
            //You use Frostbite and it works on an Opponent.Then you use Flammability and Opponent loses Frostbite effect
            //and gains Flammability effect

            //convert to more simple task 
            //You use Flammability and it works on an Opponent and that one loses Frostbite effect
            //and gains Flammability effect

            //given
            var skillGemFlammability = new Flammability(20, 50);

            var opponent = new Opponent { Life = 5000, ColdResistance = -20 };
            opponent.Parameters.Add(new Parameter { Name = "cold vulnerability", Count = 30 });

            var bodyArmour = new DendrobateChanged();
            bodyArmour.Slots[2].Gem = skillGemFlammability;

            var hero = new Hero();
            hero.BodyArmour = bodyArmour;

            var expectedColdResistance = 10;           //???
            var expectedFireResistance = -30;           //???
            var expectedNameParamert = "fire vulnerability";

            //when
            var skill = hero.GetSkills()[0];
            opponent.GetHit(skill);

            //then
            Assert.AreEqual(expectedColdResistance, opponent.ColdResistance);
            Assert.AreEqual(expectedFireResistance, opponent.FireResistance);
            Assert.AreEqual(expectedNameParamert, opponent.Parameters[0].Name);
        }
    }
}
