﻿using Frostbolt.Domain.Entities;
using NUnit.Framework;

namespace Frostbolt.Tests.Domain
{
    public class SupportGemsTest
    {
        [TestCase(1, 1, 1)]
        [TestCase(15, 5, 230.62)]
        [TestCase(20, 50, 500)]
        public void AddedLightningDamageSupport_CheckCalculatingSkills_PositiveResult(int lvl, int quality, double expectedLightningDamage)
        {
            //given

            //when
            var supportGem = new AddedLightningDamageSupport(lvl, quality);

            //then
            Assert.AreEqual(expectedLightningDamage, supportGem.Parameters[0].Count);
        }

        [TestCase(1, 1, 5.02)]
        [TestCase(15, 5, 76.88)]
        [TestCase(20, 50, 125)]
        public void ChanceToPoisonSupport_CheckCalculatingSkills_PositiveResult(int lvl, int quality, double expectedChaosDamage)
        {
            //given

            //when
            var supportGem = new ChanceToPoisonSupport(lvl, quality);

            //then
            Assert.AreEqual(expectedChaosDamage, supportGem.Parameters[1].Count);
        }

        [TestCase(1, 1, 0)]
        [TestCase(15, 5, 2)]
        [TestCase(20, 50, 3)]
        public void EmpowerSupport_CheckCalculatingSkills_PositiveResult(int lvl, int quality, double expectedUpGemsLevels)
        {
            //given

            //when
            var supportGem = new EmpowerSupport(lvl, quality);

            //then
            Assert.AreEqual(expectedUpGemsLevels, supportGem.Parameters[0].Count);
        }

        [TestCase(1, 1, 0.34)]
        [TestCase(15, 5, 0.31)]
        [TestCase(20, 50, 0.08)]
        public void GreaterMultipleProjectilesSupport_CheckCalculatingSkills_PositiveResult(int lvl, int quality, double expectedDamageReduced)
        {
            //given

            //when
            var supportGem = new GreaterMultipleProjectilesSupport(lvl, quality);

            //then
            Assert.AreEqual(expectedDamageReduced, supportGem.Parameters[3].Count);
        }

        [TestCase(1, 1, 0.61)]
        [TestCase(15, 5, 0.77)]
        [TestCase(20, 50, 1)]
        public void SpellCascadeSupport_CheckCalculatingSkills_PositiveResult(int lvl, int quality, double expectedAllDamageMultiplier)
        {
            //given

            //when
            var supportGem = new SpellCascadeSupport(lvl, quality);

            //then
            Assert.AreEqual(expectedAllDamageMultiplier, supportGem.Parameters[2].Count);
        }

        [TestCase(1, 1, 0.9, 0.53)]
        [TestCase(15, 5, 0.92, 0.39)]
        [TestCase(20, 50, 1.12, 0.34)]
        public void SpellEchoSupport_CheckCalculatingSkills_PositiveResult(int lvl, int quality, double expectedAllDamageMultiplier, double expectedCastSpeed)
        {
            //given

            //when
            var supportGem = new SpellEchoSupport(lvl, quality);

            //then
            Assert.AreEqual(expectedAllDamageMultiplier, supportGem.Parameters[2].Count);
        }

        //[TestCase(1, 1, 1)]
        ////[TestCase(15, 5, 230.62)]
        ////[TestCase(20, 50, 500)]
        //public void AddEffect_CheckCalculatingSkills_PositiveResult(int lvl, int quality, double expectedLightningDamage)
        //{
        //    //given
        //    //Frostbolt Damage = 3300
        //    var skillGem = new Frostbolt.Domain.Entities.Frostbolt(20, 50);
        //    skillGem.Parameters.Add(new Parameter { Name = "cold vulnerability", Count = 20 });

        //    //when
        //    var supportGem = new AddedLightningDamageSupport(lvl, quality);
        //    supportGem.AddEffect(skillGem);
        //    //then
        //    Assert.AreEqual(expectedLightningDamage, supportGem.Parameters[0].Count);
        //}

    }
}
