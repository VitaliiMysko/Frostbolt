﻿using Frostbolt.Domain.Entities;
using NUnit.Framework;
using System.Linq;

namespace Frostbolt.Tests.Domain
{
    public class SkillGemsTest
    {
        [TestCase(1, 1, 207, 6)]
        [TestCase(15, 5, 1391, 20)]
        [TestCase(20, 50, 3300, 25)]
        public void Frostbolt_CheckCalculatingSkills_PositiveResult(int lvl, int quality, int expectedDamage, double expectedManaCost)
        {
            //given

            //when
            var skillGem = new Frostbolt.Domain.Entities.Frostbolt(lvl, quality);

            //then
            Assert.AreEqual(expectedDamage, skillGem.Damage.Sum(g => g.Amount));
            Assert.AreEqual(expectedManaCost, skillGem.Parameters[3].Count);
        }

        [TestCase(1, 1, 55, 8.5)]
        [TestCase(15, 5, 998, 15.5)]
        [TestCase(20, 50, 2475, 18)]
        public void IceNova_CheckCalculatingSkills_PositiveResult(int lvl, int quality, int expectedDamage, double expectedManaCost)
        {
            //given

            //when
            var skillGem = new IceNova(lvl, quality);

            //then
            Assert.AreEqual(expectedDamage, skillGem.Damage.Sum(g => g.Amount));
            Assert.AreEqual(expectedManaCost, skillGem.Parameters[2].Count);
        }

        [TestCase(1, 1, 30, 49, 6.14)]
        [TestCase(15, 5, 30, 491, 8.14)]
        [TestCase(20, 50, 30, 952, 8.86)]
        public void PoisonousConcoction_CheckCalculatingSkills_PositiveResult(int lvl, int quality, int flaskPower, int expectedDamage, double expectedManaCost)
        {
            //given

            //when
            var skillGem = new PoisonousConcoction(lvl, flaskPower);

            //then
            Assert.AreEqual(expectedDamage, skillGem.Damage.Sum(g => g.Amount));
            Assert.AreEqual(expectedManaCost, skillGem.Parameters[2].Count);

        }

        [TestCase(1, 1, 35, 14.5, 0)]
        [TestCase(15, 5, 105, 21.5, 0.43)]
        [TestCase(20, 50, 130, 24, 0.71)]
        public void Tornado_CheckCalculatingSkills_PositiveResult(int lvl, int quality, int expectedDamage, double expectedManaCost, double expectedMoveSpeed)
        {
            //given

            //when
            var skillGem = new Tornado(lvl, quality);

            //then
            Assert.AreEqual(expectedDamage, skillGem.Damage.Sum(g => g.Amount));
            Assert.AreEqual(expectedManaCost, skillGem.Parameters[2].Count);
            Assert.AreEqual(expectedMoveSpeed, skillGem.Parameters[1].Count);
        }

        [TestCase(1, 1, 153)]
        [TestCase(15, 5, 1072)]
        [TestCase(20, 50, 2908)]
        public void SeismicTrap_CheckCalculatingSkills_PositiveResult(int lvl, int quality, int expectedDamage)
        {
            //given

            //when
            var skillGem = new SeismicTrap(lvl, quality);

            //then
            Assert.AreEqual(expectedDamage, skillGem.Damage.Sum(g => g.Amount));
        }

        [TestCase(1, 1, 261)]
        [TestCase(15, 5, 28612)]
        [TestCase(20, 50, 96375)]
        public void WildStrike_CheckCalculatingSkills_PositiveResult(int lvl, int quality, int expectedDamage)
        {
            //given

            //when
            var skillGem = new WildStrike(lvl, quality);

            //then
            Assert.AreEqual(expectedDamage, skillGem.Damage.Sum(g => g.Amount));
        }

    }
}
